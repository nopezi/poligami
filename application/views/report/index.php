<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body">
            <!-- Add rows table -->
            <section id="add-row">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Report</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table add-rows">
                                            <thead>
                                                <tr>
                                                    <th>User Id</th>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Gender</th>
                                                    <th>Age</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th>126489593</th>
                                                    <th>
                                                        <div class="avatar mr-1"><img src="<?= base_url(); ?>asset/app-assets/images/profile/user-uploads/user-01.jpg" alt="avtar img holder" height="45" width="45"></div>
                                                    </th>
                                                    <th>Agil</th>
                                                    <th>Male</th>
                                                    <th>26</th>
                                                    <th>
                                                        <a class="btn btn-primary text-white">View</a>
                                                        <a class="btn btn-secondary text-white">Block</a>
                                                        <a class="btn btn-danger text-white">Delete</a>
                                                    </th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Add rows table -->
        </div>
    </div>
</div>
<!-- END: Content-->