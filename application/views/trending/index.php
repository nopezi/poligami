<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body">

            <?php if (!empty($trending)): ?>

                <section id="component-swiper-centered-slides">
                    <div class="card bg-transparent shadow-none">
                        <div class="card-header">
                            <h1 class="card-title">Top 10 Trending Users</h1>
                        </div>

                        <div class="card-content">
                            <div class="card-body">
                                <div class="swiper-centered-slides swiper-container p-1">

                                    <div class="swiper-wrapper">
                                        <?php $i=1; foreach ($trending as $trd):
                                            if ($i == 1): ?>
                                                <div class=" swiper-slide swiper-shadow rounded col-xl-3 col-md-6 col-sm-12 profile-card-2">
                                                    <div class="card">
                                                        <div class="card-header mx-auto pb-0">
                                                            <div class="row m-0">
                                                                <div class="col-sm-12 text-center">
                                                                    <h1>#<?= $trd['number'] ?></h1>
                                                                </div>
                                                                <div class="col-sm-12 text-center">
                                                                    <h4 class=""><?= $trd['fullname'] ?></h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="card-content">
                                                            <div class="card-body text-center mx-auto">
                                                                <div class="avatar avatar-xl">
                                                                    <img class="img-fluid" src="<?= base_url('images/users/') . $trd['userimage']; ?>" alt="img placeholder">
                                                                </div>
                                                                <div class="d-flex mt-2">
                                                                    <div class="col-sm-12 text-center">
                                                                        <p class="font-weight-bold font-medium-2 mb-0"><?= $trd['total_visitor'] ?></p>
                                                                        <span class="">Visitors</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif ?>

                                            <?php if ($i++ == 10) break?>

                                        <?php endforeach?>

                                    </div>

                                    <!-- Add Arrows -->
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            <?php endif?>

            <!-- Add rows table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Trending User</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table add-rows">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>User Id</th>
                                                    <th>Trending</th>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Gender</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php $i = 1;
                                                foreach ($trending as $trd) { ?>
                                                    <tr>
                                                        <th><?= $i ?></th>
                                                        <th><?= $trd['userid']; ?></th>
                                                        <th>#<?= $trd['number']; ?></th>
                                                        <th>
                                                            <div class="avatar mr-1"><img src="<?= base_url('images/users/') . $trd['userimage']; ?>" alt="avtar img holder" height="45" width="45"></div>
                                                        </th>
                                                        <th><?= $trd['fullname']; ?></th>
                                                        <th><?= $trd['gender']; ?></th>
                                                        <th>
                                                            <?php if ($trd['statususer'] == 1) { ?>
                                                                <h1 class="badge badge-success">Active</h1>
                                                            <?php } else { ?>
                                                                <h1 class="badge badge-secondary">Blocked</h1>
                                                            <?php } ?>
                                                        </th>
                                                        <th>
                                                            <span>
                                                                <a href="<?= base_url(); ?>user/detailusers/<?= $trd['userid'] ?>">
                                                                    <i class="feather icon-eye text-primary"></i>
                                                                </a>
                                                            </span>
                                                        </th>
                                                    </tr>

                                                <?php $i++;
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>
<!-- END: Content-->