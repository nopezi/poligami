<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Applications that support the search for people who have the same interests, or can find a soulmate too">
    <meta name="keywords" content="ourdevelops, dating, soulmate, interest, hobby, comunity, trending, tinder, girlfriends, boyfriends">
    <meta name="author" content="ourdevelops">
    <title>Odate Admin</title>
    <link rel="apple-touch-icon" href="<?= base_url(); ?>asset/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url(); ?>asset/app-assets/images/ico/logoweb.png">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/vendors/css/pickers/pickadate.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/node_modules/dropify/dist/css/dropify.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/vendors/css/extensions/swiper.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/vendors/css/extensions/toastr.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/vendors/css/forms/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/node_modules/summernote/dist/summernote-bs4.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/css/ourdevelops/apexcharts.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/vendors/css/tables/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/vendors/css/tables/dataTables.checkboxes.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/css/components.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/css/core/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/css/core/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/css/pages/users.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/css/pages/app-user.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/css/plugins/swiper.css">
    <link rel="stylesheet" href="<?= base_url(); ?>asset/app-assets/css/ourdevelops/intlTelInput.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/css/plugins/toastr.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/app-assets/css/pages/data-list-view.css">

    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

    <input type="hidden" id="base" value="<?php echo base_url(); ?>">

    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon feather icon-menu"></i></a></li>
                        </ul>
                    </div>
                    <ul class="nav navbar-nav float-right">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon feather icon-maximize"></i></a></li>
                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                <div class="user-nav d-sm-flex d-none"><span class="user-name text-bold-600"><?= $this->session->userdata('username') ?></span><span class="user-status text-success">Online</span></div><span><img class="round" src="<?= base_url(); ?>/images/admin/<?= $this->session->userdata('image') ?>" alt="avatar" height="40" width="40"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="<?= base_url(); ?>profile"><i class="feather icon-user"></i> Edit Profile</a>
                                <div class="dropdown-divider"></div><a class="dropdown-item" href="<?= base_url(); ?>login/logout"><i class="feather icon-power"></i> Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="<?= base_url(); ?>asset/html/ltr/vertical-menu-template/index.html">
                        <span><img class="round" src="<?= base_url(); ?>asset/app-assets/images/ico/logoweb.png" alt="avatar" height="40" width="40"></span>
                        <h2 class="brand-text mb-0">odate</h2>
                    </a></li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class=" nav-item"><a href="<?= base_url(); ?>"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>

                </li>
                <li class=" nav-item"><a href="<?= base_url(); ?>trending"><i class="feather icon-zap"></i><span class="menu-title" data-i18n="Dashboard">Trending</span></a>

                </li>
                <li class=" nav-item"><a href="<?= base_url(); ?>user"><i class="feather icon-user"></i><span class="menu-title" data-i18n="Dashboard">Users</span></a>

                </li>
                <li class=" nav-item"><a href="<?= base_url(); ?>purchase"><i class="feather icon-award"></i><span class="menu-title" data-i18n="Dashboard">Subscription</span></a>

                </li>
                <li class=" nav-item"><a href="<?= base_url(); ?>subscription"><i class="feather icon-layers"></i><span class="menu-title" data-i18n="Dashboard">VIP Package</span></a>

                </li>
                <li class=" nav-item"><a href="<?= base_url(); ?>appnotification"><i class="feather icon-bell"></i><span class="menu-title" data-i18n="Dashboard">App Notification</span></a>

                </li>
                <li class=" nav-item"><a href="<?= base_url(); ?>sendemail"><i class="feather icon-send"></i><span class="menu-title" data-i18n="Dashboard">Send Email</span></a>

                </li>
                <li class=" nav-item"><a href="<?= base_url(); ?>setting/appsettings"><i class="feather icon-smartphone"></i><span class="menu-title" data-i18n="Dashboard">App Settings</span></a>

                </li>
                <li class=" nav-item"><a href="<?= base_url(); ?>setting/emailsettings"><i class="feather icon-mail"></i><span class="menu-title" data-i18n="Dashboard">Email Settings</span></a>

                </li>
                <li class=" nav-item"><a href="<?= base_url(); ?>setting/smtpsettings"><i class="feather icon-server"></i><span class="menu-title" data-i18n="Dashboard">SMTP Settings</span></a>

                </li>
                <li class=" nav-item"><a href="<?= base_url(); ?>setting/stripesettings"><i class="fa fa-cc-stripe"></i><span class="menu-title" data-i18n="Dashboard">Stripe Settings</span></a>

                </li>
                <li class=" nav-item"><a href="<?= base_url(); ?>setting/paypalsettings"><i class="fa fa-paypal"></i><span class="menu-title" data-i18n="Dashboard">Paypal Settings</span></a>

                </li>



            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->