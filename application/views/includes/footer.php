<div class="sidenav-overlay"></div>
<div class="drag-target"></div>


<?php if ($this->session->flashdata('success')) { ?>

    <section id="animation">

        <div id="type-success">

            <input type="hidden" id="desctoast" value="<?php echo $this->session->flashdata('success'); ?>" />
        </div>
    </section>
<?php } ?>

<?php if ($this->session->flashdata('danger')) { ?>

    <section id="animation">

        <div id="type-danger">

            <input type="hidden" id="desctoast" value="<?php echo $this->session->flashdata('danger'); ?>" />
        </div>
    </section>
<?php } ?>

<?php if ($this->session->flashdata('demo')) { ?>

    <section id="animation">

        <div id="type-danger">

            <input type="hidden" id="desctoast" value="<?php echo $this->session->flashdata('demo'); ?>" />
        </div>
    </section>
<?php } ?>

<!-- BEGIN: Footer-->
<footer class="footer footer-static footer-light">
    <p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2020<a class="text-bold-800 grey darken-2" href="#">Ourdevelops,</a>All rights Reserved</span><span class="float-md-right d-none d-md-block">Hand-crafted & Made with<i class="feather icon-heart pink"></i></span>

    </p>
</footer>
<!-- END: Footer-->


<!-- BEGIN: Vendor JS-->
<script src="<?= base_url(); ?>asset/app-assets/vendors/js/vendors.min.js"></script>
<script src="<?= base_url(); ?>asset/app-assets/vendors/js/extensions/toastr.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="<?= base_url(); ?>asset/app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="<?= base_url(); ?>asset/app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="<?= base_url(); ?>asset/app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
<script src="<?= base_url(); ?>asset/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="<?= base_url(); ?>asset/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
<script src="<?= base_url(); ?>asset/app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
<script src="<?= base_url(); ?>asset/app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
<script src="<?= base_url(); ?>asset/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>asset/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
<script src="<?= base_url(); ?>asset/app-assets/vendors/js/charts/apexcharts.min.js"></script>
<script src="<?= base_url(); ?>asset/node_modules/dropify/dist/js/dropify.min.js"></script>
<script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/intlTelInput-jquery.min.js"></script>

<script src="<?= base_url(); ?>asset/app-assets/vendors/js/tables/datatable/dataTables.select.min.js"></script>
<script src="<?= base_url(); ?>asset/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>


<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="<?= base_url(); ?>asset/app-assets/js/core/app-menu.js"></script>
<script src="<?= base_url(); ?>asset/app-assets/js/core/app.js"></script>
<script src="<?= base_url(); ?>asset/app-assets/js/scripts/components.js"></script>

<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<?php if ($view == "dashboard") { ?>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/dashboard.js"></script>
<?php } ?>

<?php if ($view == "editadmin") { ?>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/dropify.js"></script>
<?php } ?>

<?php if ($view == "trending") { ?>
    <script src="<?= base_url(); ?>asset/app-assets/vendors/js/extensions/swiper.min.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/trending.js"></script>
<?php } ?>

<?php if ($view == "detailuser") { ?>
    <script src="<?= base_url(); ?>asset/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/forms/select/form-select2.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/dropify.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/modal/components-modal.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/countrycode.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/detailuser.js"></script>
<?php } ?>

<?php if ($view == "adduser") { ?>
    <script src="<?= base_url(); ?>asset/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/forms/select/form-select2.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/dropify.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/adduser.js"></script>
<?php } ?>

<?php if ($view == "appsettings") { ?>
    <script src="<?= base_url(); ?>asset/node_modules/summernote/dist/summernote-bs4.min.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/dropify.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/forms/select/form-select2.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/quilleditor.js"></script>
<?php } ?>

<?php if ($view == "addsubscription") { ?>
    <script src="<?= base_url(); ?>asset/app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="<?= base_url(); ?>asset/node_modules/summernote/dist/summernote-bs4.min.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/dropify.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/forms/select/form-select2.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/quilleditor.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/duit.js"></script>
<?php } ?>

<?php if ($view == "sendemail") { ?>
    <script src="<?= base_url(); ?>asset/node_modules/summernote/dist/summernote-bs4.min.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/forms/select/form-select2.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/quilleditor.js"></script>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/sendemail.js"></script>
<?php } ?>

<script src="<?= base_url(); ?>asset/app-assets/js/scripts/datatables/datatable.js"></script>
<script src="<?= base_url(); ?>asset/app-assets/js/scripts/ui/data-list-view.js"></script>




<?php if ($this->session->flashdata('success')) { ?>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/toastrsuccess.js"></script>
<?php } ?>

<?php if ($this->session->flashdata('danger') || $this->session->flashdata('demo')) { ?>
    <script src="<?= base_url(); ?>asset/app-assets/js/scripts/ourdevelops/toastrerror.js"></script>
<?php } ?>
<!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>