<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body">
            <!-- Dashboard Analytics Start -->
            <section id="dashboard-analytics">
                <div class="row">
                    <div class="col-md-4 col-12">
                        <div class="card">
                            <div class="card-header d-flex flex-column align-items-start pb-0">
                                <div class="avatar bg-rgba-primary p-50 m-0">
                                    <div class="avatar-content">
                                        <i class="feather icon-user text-primary font-medium-5"></i>
                                    </div>
                                </div>
                                <h2 class="text-bold-700 mt-1 mb-25"><?= count($users); ?></h2>
                                <p class="mb-0">Total User</p>
                            </div>
                            <div class="card-content">
                                <div id="chart1"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="card">
                            <div class="card-header d-flex flex-column align-items-start pb-0">
                                <div class="avatar bg-rgba-warning p-50 m-0">
                                    <div class="avatar-content">
                                        <i class="feather icon-users text-warning font-medium-5"></i>
                                    </div>
                                </div>
                                <h2 class="text-bold-700 mt-1 mb-25"><?= count($match); ?></h2>
                                <p class="mb-0">Match User</p>
                            </div>
                            <div class="card-content">
                                <div id="chart2"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="card">
                            <div class="card-header d-flex flex-column align-items-start pb-0">
                                <div class="avatar bg-rgba-success p-50 m-0">
                                    <div class="avatar-content">
                                        <i class="feather icon-award text-success font-medium-5"></i>
                                    </div>
                                </div>
                                <h2 class="text-bold-700 mt-1 mb-25"><?= $vip; ?></h2>
                                <p class="mb-0">VIP User</p>
                            </div>
                            <div class="card-content">
                                <div id="chart3"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-8 col-md-6 col-12">
                        <div class="card">
                            <div class="card-header d-flex justify-content-between align-items-end">
                                <h4 class="card-title">Subscription Income</h4>
                                <p class="font-medium-5 mb-0">
                                </p>
                            </div>
                            <div class="card-content">
                                <div class="card-body pb-0">
                                    <div class="d-flex justify-content-start">
                                        <div>
                                            <p class="mb-50 text-bold-600">Total</p>
                                            <h2 class="text-bold-400">
                                                <sup class="font-medium-1"><?= $currency ?></sup>
                                                <span class="text-success"><?= number_format($totalamount / 100, 2, ".", ".") ?></span>
                                            </h2>
                                        </div>
                                        <div class="mr-2 ml-2">
                                            <p class="mb-50 text-bold-600">PayPal</p>
                                            <h2 class="text-bold-400">
                                                <sup class="font-medium-1"><?= $currency ?></sup>
                                                <span><?= number_format($totalpaypal / 100, 2, ".", ".") ?></span>
                                            </h2>
                                        </div>
                                        <div>
                                            <p class="mb-50 text-bold-600">Stripe</p>
                                            <h2 class="text-bold-400">
                                                <sup class="font-medium-1"><?= $currency ?></sup>
                                                <span><?= number_format($totalstripe / 100, 2, ".", ".") ?></span>
                                            </h2>
                                        </div>
                                    </div>
                                    <div id="revenue-chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="card">
                            <div class="card-header d-flex justify-content-between align-items-end">
                                <h4 class="mb-0">Matches Meter</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body px-0 pb-0">
                                    <div id="goal-overview-chart" class="mt-75"></div>
                                    <div class="row text-center mx-0">
                                        <div class="col-6 border-top border-right d-flex align-items-between flex-column py-1">
                                            <p class="mb-50">Match</p>
                                            <p class="font-large-1 text-bold-700"><?= $totalmatch ?></p>
                                        </div>
                                        <div class="col-6 border-top d-flex align-items-between flex-column py-1">
                                            <p class="mb-50">Like</p>
                                            <p class="font-large-1 text-bold-700"><?= $totalprogress ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
            <!-- Dashboard Analytics end -->

            <!-- Basic tabs start -->
            <section id="basic-tabs-components">
                <div class="row">
                    <div class="col">
                        <div class="card overflow-hidden">
                            <div class="card-header">
                                <h4 class="card-title">User Activity</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link d-flex align-items-center active" id="allactivity-tab" data-toggle="tab" href="#allactivity" aria-controls="allactivity" role="tab" aria-selected="true">
                                                <i class="feather icon-activity mr-25"></i>
                                                <span class="d-none d-sm-block text-center">All Activity</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link d-flex align-items-center" id="matchactivity-tab" data-toggle="tab" href="#matchactivity" aria-controls="macthactivity" role="tab" aria-selected="false">
                                                <i class="fa fa-exchange mr-25"></i>
                                                <span class="d-none d-sm-block text-center">Match Activity</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link d-flex align-items-center" id="likeactivity-tab" data-toggle="tab" href="#likeactivity" aria-controls="likeactivity" role="tab" aria-selected="false">
                                                <i class="fa fa-heart mr-25"></i>
                                                <span class="d-none d-sm-block text-center">Like Activity</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link d-flex align-items-center" id="visitactivity-tab" data-toggle="tab" href="#visitactivity" aria-controls="visitactivity" role="tab" aria-selected="false">
                                                <i class="fa fa-eye mr-25"></i>
                                                <span class="d-none d-sm-block text-center">Visit Activity</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="allactivity" aria-labelledby="allactivity-tab" role="tabpanel">
                                            <!-- Add rows table -->
                                            <section id="add-row">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h4 class="card-title">All Activity</h4>
                                                            </div>
                                                            <div class="card-content">
                                                                <div class="card-body">
                                                                    <div class="table-responsive">
                                                                        <table class="table add-rows">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>No.</th>
                                                                                    <th style="width: 50px;">User</th>
                                                                                    <th></th>
                                                                                    <th class="text-center">Type</th>
                                                                                    <th class="text-right"></th>
                                                                                    <th style="width: 50px;">User</th>
                                                                                    <th class="text-center">Date</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                $i = 1;
                                                                                foreach ($activity as $mc) { ?>
                                                                                    <tr>
                                                                                        <th><?= $i ?></th>
                                                                                        <th>
                                                                                            <div class="avatar mr-25"><img href="<?= base_url(); ?>user/detailusers/<?= $mc['userid1'] ?>" src="<?= base_url(); ?>images/users/<?= $mc['userimage1']; ?>" alt="avtar img holder" height="45" width="45">
                                                                                            </div>
                                                                                        </th>
                                                                                        <th><span class="text-left"><?= $mc['namauser1']; ?></span></th>
                                                                                        <th class="text-center">
                                                                                            <?php if ($mc['status1'] == 'like') { ?>
                                                                                                <h1 class="badge badge-success text-center"><?= $mc['status1']; ?></h1>
                                                                                            <?php } elseif ($mc['status1'] == 'visitor') { ?>
                                                                                                <h1 class="badge badge-warning text-center"><?= $mc['status1']; ?></h1>
                                                                                            <?php } elseif ($mc['status1'] == 'match') { ?>
                                                                                                <h1 class="badge badge-primary text-center"><?= $mc['status1']; ?></h1>
                                                                                            <?php } ?>

                                                                                        </th>
                                                                                        <th class="text-right"><span class="text-right"><?= $mc['namauser2']; ?></span></th>
                                                                                        <th>
                                                                                            <div class="avatar mr-25"><img href="<?= base_url(); ?>user/detailusers/<?= $mc['userid2'] ?>" src="<?= base_url(); ?>images/users/<?= $mc['userimage2']; ?>" alt="avtar img holder" height="45" width="45">
                                                                                            </div>
                                                                                        </th>
                                                                                        <th class="text-center">
                                                                                            <?= $mc['datem']; ?>
                                                                                        </th>
                                                                                    </tr>
                                                                                <?php $i++;
                                                                                } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <!--/ Add rows table -->
                                        </div>
                                        <div class="tab-pane" id="matchactivity" aria-labelledby="matchactivity-tab" role="tabpanel">
                                            <!-- Add rows table -->
                                            <section id="add-row">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h4 class="card-title">Match Activity</h4>
                                                            </div>
                                                            <div class="card-content">
                                                                <div class="card-body">
                                                                    <div class="table-responsive">
                                                                        <table class="table add-rows">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>No.</th>
                                                                                    <th style="width: 50px;">User</th>
                                                                                    <th></th>
                                                                                    <th class="text-center">Type</th>
                                                                                    <th class="text-right"></th>
                                                                                    <th style="width: 50px;">User</th>
                                                                                    <th class="text-center">Date</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                $i = 1;
                                                                                foreach ($match as $mc) { ?>
                                                                                    <tr>
                                                                                        <th><?= $i ?></th>
                                                                                        <th class="justify-content-center">
                                                                                            <div class="avatar mr-25"><img src="<?= base_url(); ?>images/users/<?= $mc['userimage1']; ?>" alt="avtar img holder" height="45" width="45">
                                                                                            </div>
                                                                                        </th>
                                                                                        <th><span class="text-left"><?= $mc['namauser1']; ?></span></th>
                                                                                        <th class="text-center">
                                                                                            <h1 class="badge badge-primary text-center">Match</h1>
                                                                                        </th>
                                                                                        <th class="text-right"><span class="text-right"><?= $mc['namauser2']; ?></span></th>
                                                                                        <th>
                                                                                            <div class="avatar mr-25"><img src="<?= base_url(); ?>images/users/<?= $mc['userimage2']; ?>" alt="avtar img holder" height="45" width="45">
                                                                                            </div>
                                                                                        </th>
                                                                                        <th class="text-center">
                                                                                            <?= $mc['datem']; ?>
                                                                                        </th>
                                                                                    </tr>
                                                                                <?php $i++;
                                                                                } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <!--/ Add rows table -->
                                        </div>
                                        <div class="tab-pane" id="likeactivity" aria-labelledby="likeactivity-tab" role="tabpanel">
                                            <!-- Add rows table -->
                                            <section id="add-row">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h4 class="card-title">Like Activity</h4>
                                                            </div>
                                                            <div class="card-content">
                                                                <div class="card-body">
                                                                    <div class="table-responsive">
                                                                        <table class="table add-rows">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>No.</th>
                                                                                    <th style="width: 50px;">User</th>
                                                                                    <th></th>
                                                                                    <th class="text-center">Type</th>
                                                                                    <th class="text-right"></th>
                                                                                    <th style="width: 50px;">User</th>
                                                                                    <th class="text-center">Date</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                $i = 1;
                                                                                foreach ($like as $lk) { ?>
                                                                                    <tr>
                                                                                        <th><?= $i ?></th>
                                                                                        <th class="justify-content-center">
                                                                                            <div class="avatar mr-25"><img src="<?= base_url(); ?>images/users/<?= $lk['userimage1']; ?>" alt="avtar img holder" height="45" width="45">
                                                                                            </div>
                                                                                        </th>
                                                                                        <th><span class="text-left"><?= $lk['namauser1']; ?></span></th>
                                                                                        <th class="text-center">
                                                                                            <h1 class="badge badge-success text-center">Like</h1>
                                                                                        </th>
                                                                                        <th class="text-right"><span class="text-right"><?= $lk['namauser2']; ?></span></th>
                                                                                        <th>
                                                                                            <div class="avatar mr-25"><img src="<?= base_url(); ?>images/users/<?= $lk['userimage2']; ?>" alt="avtar img holder" height="45" width="45">
                                                                                            </div>
                                                                                        </th>
                                                                                        <th class="text-center">
                                                                                            <?= $lk['datem']; ?>
                                                                                        </th>
                                                                                    </tr>
                                                                                <?php $i++;
                                                                                } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <!--/ Add rows table -->
                                        </div>
                                        <div class="tab-pane" id="visitactivity" aria-labelledby="visitactivity-tab" role="tabpanel">
                                            <!-- Add rows table -->
                                            <section id="add-row">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h4 class="card-title">Visit Activity</h4>
                                                            </div>
                                                            <div class="card-content">
                                                                <div class="card-body">
                                                                    <div class="table-responsive">
                                                                        <table class="table add-rows">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>No.</th>
                                                                                    <th style="width: 50px;">User</th>
                                                                                    <th></th>
                                                                                    <th class="text-center">Type</th>
                                                                                    <th class="text-right"></th>
                                                                                    <th style="width: 50px;">User</th>
                                                                                    <th class="text-center">Date</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                $i = 1;
                                                                                foreach ($visit as $vst) { ?>
                                                                                    <tr>
                                                                                        <th><?= $i ?></th>
                                                                                        <th class="justify-content-center">
                                                                                            <div class="avatar mr-25"><img src="<?= base_url(); ?>images/users/<?= $vst['userimage1']; ?>" alt="avtar img holder" height="45" width="45">
                                                                                            </div>
                                                                                        </th>
                                                                                        <th><span class="text-left"><?= $vst['namauser1']; ?></span></th>
                                                                                        <th class="text-center">
                                                                                            <h1 class="badge badge-warning text-center">Visit</h1>
                                                                                        </th>
                                                                                        <th class="text-right"><span class="text-right"><?= $vst['namauser2']; ?></span></th>
                                                                                        <th>
                                                                                            <div class="avatar mr-25"><img src="<?= base_url(); ?>images/users/<?= $vst['userimage2']; ?>" alt="avtar img holder" height="45" width="45">
                                                                                            </div>
                                                                                        </th>
                                                                                        <th class="text-center">
                                                                                            <?= $vst['datev']; ?>
                                                                                        </th>
                                                                                    </tr>
                                                                                <?php $i++;
                                                                                } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <!--/ Add rows table -->
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
            <!-- Basic Tag Input end -->

        </div>

    </div>
</div>
</div>
<!-- END: Content-->