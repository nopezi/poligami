<?php

class Purchase_model extends CI_model
{
    public function getpurchasetotalamount()
    {
        $this->db//->select('id_purchase, SUM(amount) as totalamount')
                 ->select('id_purchase')
                 ->select('SUM(amount) as totalamount')
                 ->from('purchaseuser')
                 ->where('status', 1)
                 ->group_by('id_purchase');;
        $data = $this->db->get()->first_row();
        if (!empty($data)) {
            return $data->totalamount;
        } else {
            return '0';
        }
    }

    public function getpaypaltotalamount()
    {
        $this->db//->select('id_purchase, SUM(amount) as totalpaypal')
                 ->select('id_purchase')
                 ->select('SUM(amount) as totalpaypal')
                 ->from("purchaseuser")
                 ->where('paymentmethod', 'paypal')
                 ->where('status', 1)
                 ->group_by('id_purchase');

        $data = $this->db->get()->first_row();
        if (!empty($data)) {
            return $data->totalpaypal;
        } else {
            return '0';
        }
    }

    public function getstripetotalamount()
    {
        $this->db->select('id_purchase, SUM(amount) as totalstripe')
                 ->from("purchaseuser")
                 ->where('paymentmethod', 'stripe')
                 ->where('status', 1)
                 ->group_by('id_purchase');

        $data = $this->db->get()->first_row();
        if (!empty($data)) {
            return $data->totalstripe;
        } else{
            return '0';
        }
    }
}
